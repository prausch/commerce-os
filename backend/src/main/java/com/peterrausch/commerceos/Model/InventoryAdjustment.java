package com.peterrausch.commerceos.Model;

import javax.persistence.*;
import java.util.List;

@Entity
public class InventoryAdjustment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String adjustmentType;
    private String note;

    @OneToMany(
            mappedBy = "inventoryAdjustment",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<InventoryAdjustmentItem> inventoryAdjustmentItemList;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAdjustmentType() {
        return adjustmentType;
    }

    public void setAdjustmentType(String adjustmentType) {
        this.adjustmentType = adjustmentType;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<InventoryAdjustmentItem> getInventoryAdjustmentItemList() {
        return inventoryAdjustmentItemList;
    }

    public void setInventoryAdjustmentItemList(List<InventoryAdjustmentItem> inventoryAdjustmentItemList) {
        this.inventoryAdjustmentItemList = inventoryAdjustmentItemList;
    }
}
