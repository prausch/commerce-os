package com.peterrausch.commerceos.Model;

import javax.persistence.*;

@Entity
public class ShipmentItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private int quantity;

    @ManyToOne
    private Product product;

    @ManyToOne
    private Shipment shipment;

    public ShipmentItem() {
        // Required for Hibernate
    }

    public ShipmentItem(Product p, int quantity) {
        this.product = p;
        this.quantity = quantity;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
