package com.peterrausch.commerceos.Model;

import com.peterrausch.commerceos.Constants.InventoryConstants;
import com.peterrausch.commerceos.DTO.Inventory.CreateShipmentRequest;

import javax.persistence.*;
import java.util.List;

@Entity
public class Shipment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String purchaseOrderId;
    private String trackingNumber;
    private String status;

    @OneToMany(
            mappedBy = "shipment",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<ShipmentItem> items;

    public Shipment() {
        // Required for Hibernate
    }

    public Shipment(String purchaseOrderId, String trackingNumber) {
        this.purchaseOrderId = purchaseOrderId;
        this.trackingNumber = trackingNumber;
        this.status = InventoryConstants.SHIPMENT_STATUS_CREATED;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPurchaseOrderId() {
        return purchaseOrderId;
    }

    public void setPurchaseOrderId(String purchaseOrderId) {
        this.purchaseOrderId = purchaseOrderId;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ShipmentItem> getItems() {
        return items;
    }

    public void setItems(List<ShipmentItem> items) {
        this.items = items;
    }
}
