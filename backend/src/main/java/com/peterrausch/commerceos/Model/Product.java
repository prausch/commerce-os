package com.peterrausch.commerceos.Model;

import com.peterrausch.commerceos.DTO.Product.CreateProductRequest;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    private String sku;
    private String description;
    private int availableQuantity;
    private BigDecimal cost;
    private BigDecimal salePrice;
    private boolean deleted;

    public Product() {

    }

    public Product(CreateProductRequest createProductRequest) {
        this.name = createProductRequest.name;
        this.sku = createProductRequest.sku;
        this.description = createProductRequest.description;
        this.availableQuantity = 0;
        this.cost = createProductRequest.cost;
        this.salePrice = createProductRequest.price;
        this.deleted = false;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public BigDecimal getSalePrice() {
        return salePrice;
    }

    public int getAvailableQuantity() {
        return availableQuantity;
    }

    public void setAvailableQuantity(int availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

    public void setSalePrice(BigDecimal salePrice) {
        this.salePrice = salePrice;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
