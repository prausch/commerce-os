package com.peterrausch.commerceos.Model;

import com.peterrausch.commerceos.DTO.Inventory.ShipmentProduct;

import javax.persistence.*;

@Entity
public class InventoryAdjustmentItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToOne
    private Product product;

    @ManyToOne
    private InventoryAdjustment inventoryAdjustment;

    public InventoryAdjustmentItem() {
        // Required for Hibernate
    }

    public InventoryAdjustmentItem(Product product, int quantity) {
    }
}
