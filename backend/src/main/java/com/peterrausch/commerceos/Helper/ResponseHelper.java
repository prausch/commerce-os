package com.peterrausch.commerceos.Helper;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;

public class ResponseHelper {

    public static ResponseEntity<HashMap<String, Object>> createErrorResponse(String message)  {
        HashMap<String, Object> response = new HashMap<>();
        response.put("error", "true");
        response.put("message", message);

        // TODO: Log an error in whatever logger? Undecided if I want to use just log4j.

        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    public static ResponseEntity<Object> createCreatedResponse(Object object) {
        return new ResponseEntity<>(object, HttpStatus.CREATED);
    }
}
