package com.peterrausch.commerceos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CommerceosApplication {

	public static void main(String[] args) {
		SpringApplication.run(CommerceosApplication.class, args);
	}

}
