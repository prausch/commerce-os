package com.peterrausch.commerceos.Service;

import com.peterrausch.commerceos.DTO.Inventory.CreateInventoryAdjustmentRequest;
import com.peterrausch.commerceos.DTO.Inventory.CreateShipmentRequest;
import com.peterrausch.commerceos.Exception.InvalidShipmentRequest;
import com.peterrausch.commerceos.DTO.Inventory.ShipmentProduct;
import com.peterrausch.commerceos.Exception.ProductNotFoundException;
import com.peterrausch.commerceos.Model.*;
import com.peterrausch.commerceos.Repository.InventoryAdjustmentItemRepository;
import com.peterrausch.commerceos.Repository.InventoryAdjustmentRepository;
import com.peterrausch.commerceos.Repository.ShipmentItemRepository;
import com.peterrausch.commerceos.Repository.ShipmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class InventoryService {

    private ProductService productService;
    private ShipmentRepository shipmentRepository;
    private ShipmentItemRepository shipmentItemRepository;
    private InventoryAdjustmentRepository inventoryAdjustmentRepository;
    private InventoryAdjustmentItemRepository inventoryAdjustmentItemRepository;

    @Autowired
    public InventoryService(ProductService productService,
                            ShipmentRepository shipmentRepository,
                            ShipmentItemRepository shipmentItemRepository,
                            InventoryAdjustmentRepository inventoryAdjustmentRepository,
                            InventoryAdjustmentItemRepository inventoryAdjustmentItemRepository) {
        this.productService = productService;
        this.shipmentRepository = shipmentRepository;
        this.shipmentItemRepository = shipmentItemRepository;
    }

    public Shipment createShipment(CreateShipmentRequest createShipmentRequest) throws ProductNotFoundException, InvalidShipmentRequest {

        // Shouldn't have to validate if a product exists more than once in the array - UI shouldn't allow that.
        List<ShipmentItem> shipmentItems = new ArrayList<>();
        Shipment shipment = new Shipment(createShipmentRequest.purchaseOrderId, createShipmentRequest.trackingNumber);

        if (createShipmentRequest.products.size() == 0) {
            throw new InvalidShipmentRequest("You can't create a shipment with 0 products.");
        }

        // Loop through each ShipmentProduct DTO and create a ShipmentItem (Database entity)
        for (ShipmentProduct product : createShipmentRequest.products) {
            Product productEntity = this.productService.getProductById(product.productId);

            if (productEntity == null) {
                throw new ProductNotFoundException("Product ID (" + product.productId + ") not found.");
            }


            ShipmentItem shipmentItemDatabaseEntity = new ShipmentItem(productEntity, product.quantity);
            shipmentItems.add(shipmentItemDatabaseEntity);

            // Save shipment item data to memory so we can handle failure if need be
            this.shipmentItemRepository.save(shipmentItemDatabaseEntity);
        }

        shipment.setItems(shipmentItems);

        // Persist items to database
        this.shipmentRepository.saveAndFlush(shipment);
        this.shipmentItemRepository.flush();

        return shipment;
    }

    public InventoryAdjustment createInventoryAdjustment(CreateInventoryAdjustmentRequest createInventoryAdjustmentRequest) throws ProductNotFoundException {

        InventoryAdjustment inventoryAdjustment = new InventoryAdjustment();
        List<InventoryAdjustmentItem> adjustmentItems = new ArrayList<>();

        for (ShipmentProduct product : createInventoryAdjustmentRequest.productAdjustments) {
            Product productEntity = this.productService.getProductById(product.productId);

            if (productEntity == null) {
                throw new ProductNotFoundException("Product not found: " + product.productId);
            }

            InventoryAdjustmentItem adjustmentItem = new InventoryAdjustmentItem(productEntity, product.quantity);

            this.inventoryAdjustmentItemRepository.save(adjustmentItem);
            adjustmentItems.add(adjustmentItem);
        }

        inventoryAdjustment.setInventoryAdjustmentItemList(adjustmentItems);
        this.inventoryAdjustmentRepository.saveAndFlush(inventoryAdjustment);
        this.inventoryAdjustmentItemRepository.flush();

        return inventoryAdjustment;
    }

}
