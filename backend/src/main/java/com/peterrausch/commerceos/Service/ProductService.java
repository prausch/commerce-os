package com.peterrausch.commerceos.Service;

import com.peterrausch.commerceos.DTO.Product.CreateProductRequest;
import com.peterrausch.commerceos.Exception.SkuExistsException;
import com.peterrausch.commerceos.Model.Product;
import com.peterrausch.commerceos.Repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    private ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> getAllProducts() {
        return this.productRepository.findAll();
    }

    public Product getProductById(long id) {
        if (!this.productRepository.existsById(id)) {
            return null;
        }
        return this.productRepository.findById(id).get();
    }

    public Product getProductBySku(String sku) {
        return this.productRepository.getProductBySku(sku);
    }

    public Product createProduct(CreateProductRequest createProductRequest) throws SkuExistsException {
        Product product = new Product(createProductRequest);

        if (this.getProductBySku(product.getSku()) != null) {
            throw new SkuExistsException("The sku you entered (" + product.getSku() + ") already exists");
        }

        this.productRepository.saveAndFlush(product);

        return product;
    }
}
