package com.peterrausch.commerceos.Service;

import com.peterrausch.commerceos.Constants.OrderConstants;
import com.peterrausch.commerceos.DTO.Customer.CreateCustomerRequest;
import com.peterrausch.commerceos.DTO.Order.AddItemsToOrderRequest;
import com.peterrausch.commerceos.Exception.OrderNotFoundException;
import com.peterrausch.commerceos.Exception.ProductNotFoundException;
import com.peterrausch.commerceos.Model.Customer;
import com.peterrausch.commerceos.Model.OrderItem;
import com.peterrausch.commerceos.Model.Product;
import com.peterrausch.commerceos.Model.SalesOrder;
import com.peterrausch.commerceos.Repository.OrderItemRepository;
import com.peterrausch.commerceos.Repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderService {

    private OrderRepository orderRepository;
    private OrderItemRepository orderItemRepository;
    private ProductService productService;

    @Autowired
    public OrderService(OrderRepository orderRepository, OrderItemRepository orderItemRepository, ProductService productService) {
        this.orderRepository = orderRepository;
        this.orderItemRepository = orderItemRepository;
        this.productService = productService;
    }

    public SalesOrder getSalesOrderById(long id) throws OrderNotFoundException {
        if (!this.orderRepository.existsById(id)) {
            throw new OrderNotFoundException("Sales order not found by ID: " + id);
        }

        return this.orderRepository.findById(id).get();
    }

    public SalesOrder createNewOrder(Customer customer) {
        SalesOrder salesOrder = new SalesOrder();

        this.orderRepository.saveAndFlush(salesOrder);

        return salesOrder;
    }

    public SalesOrder addItemsToOrder(AddItemsToOrderRequest addItemsToOrderRequest) throws OrderNotFoundException, ProductNotFoundException {
        SalesOrder order = this.getSalesOrderById(addItemsToOrderRequest.orderId);

        List<OrderItem> orderItems = new ArrayList<>();
        // Loop through each item, find the item, then add it to the order.
        for (Long productId : addItemsToOrderRequest.productIds) {
            Product product = this.productService.getProductById(productId);

            if (product == null) {
                throw new ProductNotFoundException("Product not found: " + productId);
            }

            OrderItem orderItem = new OrderItem(order, product);

            this.orderItemRepository.save(orderItem);
            orderItems.add(orderItem);
        }

        order.setItems(orderItems);

        this.orderItemRepository.flush();
        this.orderRepository.saveAndFlush(order);

        this.repriceOrder(order);

        return order;
    }

    public void repriceOrder(SalesOrder order) {

        BigDecimal totalPrice = new BigDecimal(0.00);

        // Loop through each order item
        for (OrderItem orderItem : order.getItems()) {
            // Add base sale price of the item
            BigDecimal itemPrice = totalPrice.add(orderItem.getProduct().getSalePrice());

            if (orderItem.getDiscount() != null) {
                BigDecimal totalDiscountAmount = new BigDecimal(0.00);
                if (orderItem.getDiscount().getDiscountType().equalsIgnoreCase(OrderConstants.DISCOUNT_DOLLAR_AMOUNT)) {
                    totalDiscountAmount.add(orderItem.getDiscount().getValue());
                    itemPrice.subtract(orderItem.getDiscount().getValue());
                } else if(orderItem.getDiscount().getDiscountType().equalsIgnoreCase(OrderConstants.DISCOUNT_PERCENTAGE)) {
                    // These will be stored in the database as "10" representing 10 percent if it's a percentage discount
                    // Take item price multiply by (discount amount / 100)
                    BigDecimal discountAmount = orderItem.getDiscount().getValue().divide(new BigDecimal(100.00));
                    BigDecimal discountValue = discountAmount.multiply(orderItem.getProduct().getSalePrice());

                    totalDiscountAmount.add(discountValue);
                    itemPrice.subtract(discountValue);
                }
            }

            totalPrice = totalPrice.add(itemPrice);
        }
    }
}
