package com.peterrausch.commerceos.Service;

import com.peterrausch.commerceos.DTO.Customer.CreateCustomerRequest;
import com.peterrausch.commerceos.Model.Customer;
import com.peterrausch.commerceos.Repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

    private CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public Customer getCustomerById(long id) {
        if (!this.customerRepository.existsById(id)) {
            return null;
        }

        return this.customerRepository.findById(id).get();
    }

    public Customer createCustomer(CreateCustomerRequest customerRequest) {
        Customer c = new Customer(customerRequest);

        // TODO: Add email validation - make sure it doesn't exist already (maybe that can be done on the front end with a isEmailValid endpoint?)
        this.customerRepository.saveAndFlush(c);

        return c;
    }

    public List<Customer> getAllCustomers() {
        return this.customerRepository.findAll();
    }
}
