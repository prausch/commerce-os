package com.peterrausch.commerceos.Constants;

public class InventoryConstants {

    // Shipment Status
    public static String SHIPMENT_STATUS_CREATED = "SHIPMENT_CREATED";
    public static String SHIPMENT_STATUS_PROCESSING = "SHIPMENT_PROCESSING";
    public static String SHIPMENT_STATUS_RECEIVED = "SHIPMENT_RECEIVED";
}
