package com.peterrausch.commerceos.Constants;

public class OrderConstants {

    // Discount Types
    public static String DISCOUNT_PERCENTAGE = "PERCENTAGE";
    public static String DISCOUNT_DOLLAR_AMOUNT = "DOLLAR_AMOUNT";
}
