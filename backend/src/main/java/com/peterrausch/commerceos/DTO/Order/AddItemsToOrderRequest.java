package com.peterrausch.commerceos.DTO.Order;

import java.util.List;

public class AddItemsToOrderRequest {
    public long orderId;
    public List<Long> productIds;
}
