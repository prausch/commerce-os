package com.peterrausch.commerceos.DTO.Inventory;

import java.util.HashMap;
import java.util.List;

public class CreateShipmentRequest {
    public String purchaseOrderId;
    public String trackingNumber;
    public List<ShipmentProduct> products;
}
