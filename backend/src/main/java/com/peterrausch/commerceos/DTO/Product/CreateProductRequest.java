package com.peterrausch.commerceos.DTO.Product;

import java.math.BigDecimal;

public class CreateProductRequest {
    public String name;
    public String sku;
    public String description;
    public BigDecimal cost;
    public BigDecimal price;

}
