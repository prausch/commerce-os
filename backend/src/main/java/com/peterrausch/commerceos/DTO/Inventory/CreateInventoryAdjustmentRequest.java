package com.peterrausch.commerceos.DTO.Inventory;

import java.util.List;

public class CreateInventoryAdjustmentRequest {
    public String adjustmentReason;
    public String note;
    public List<ShipmentProduct> productAdjustments;
}
