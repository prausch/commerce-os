package com.peterrausch.commerceos.DTO.Customer;

public class CreateCustomerRequest {
    public String firstName;
    public String lastName;
    public String address;
    public String city;
    public String state;
    public String zip;
    public String phoneNumber;
    public String emailAddress;
    public String customerType;
}
