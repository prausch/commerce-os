package com.peterrausch.commerceos.Exception;

public class InvalidShipmentRequest extends Exception {

    public InvalidShipmentRequest(String message) {
        super (message);
    }
}
