package com.peterrausch.commerceos.Exception;

public class SkuExistsException extends Exception {

    public SkuExistsException(String message) {
        super(message);
    }
}
