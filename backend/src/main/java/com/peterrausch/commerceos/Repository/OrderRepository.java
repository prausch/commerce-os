package com.peterrausch.commerceos.Repository;

import com.peterrausch.commerceos.Model.SalesOrder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<SalesOrder, Long> {
}
