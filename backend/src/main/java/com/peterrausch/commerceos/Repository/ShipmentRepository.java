package com.peterrausch.commerceos.Repository;

import com.peterrausch.commerceos.Model.Shipment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShipmentRepository extends JpaRepository<Shipment, Long> {
}
