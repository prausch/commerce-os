package com.peterrausch.commerceos.Repository;

import com.peterrausch.commerceos.Model.ShipmentItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.cdi.JpaRepositoryExtension;

public interface ShipmentItemRepository extends JpaRepository<ShipmentItem, Long> {
}
