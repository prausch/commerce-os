package com.peterrausch.commerceos.Repository;

import com.peterrausch.commerceos.Model.InventoryAdjustmentItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InventoryAdjustmentItemRepository extends JpaRepository<InventoryAdjustmentItem, Long> {
}
