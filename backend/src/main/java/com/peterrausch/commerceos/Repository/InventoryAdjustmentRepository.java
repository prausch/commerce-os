package com.peterrausch.commerceos.Repository;

import com.peterrausch.commerceos.Model.InventoryAdjustment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InventoryAdjustmentRepository extends JpaRepository<InventoryAdjustment, Long> {
}
