package com.peterrausch.commerceos.Repository;

import com.peterrausch.commerceos.Model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query("SELECT p FROM Product p WHERE p.sku = ?1")
    Product getProductBySku(String sku);
}