package com.peterrausch.commerceos.Controller;

import com.peterrausch.commerceos.DTO.Order.AddItemsToOrderRequest;
import com.peterrausch.commerceos.DTO.Order.CreateSalesOrderRequest;
import com.peterrausch.commerceos.Exception.CustomerNotFoundException;
import com.peterrausch.commerceos.Exception.OrderNotFoundException;
import com.peterrausch.commerceos.Exception.ProductNotFoundException;
import com.peterrausch.commerceos.Helper.ResponseHelper;
import com.peterrausch.commerceos.Model.Customer;
import com.peterrausch.commerceos.Model.SalesOrder;
import com.peterrausch.commerceos.Service.CustomerService;
import com.peterrausch.commerceos.Service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/v1/order")
public class OrderController {

    private OrderService orderService;
    private CustomerService customerService;

    @Autowired
    public OrderController(OrderService orderService, CustomerService customerService) {
        this.orderService = orderService;
        this.customerService = customerService;
    }

    @PostMapping("/create")
    public ResponseEntity createSalesOrder(@RequestBody CreateSalesOrderRequest createSalesOrderRequest) throws CustomerNotFoundException {
        Customer customer = this.customerService.getCustomerById(createSalesOrderRequest.customerId);

        if (customer == null) {
            throw new CustomerNotFoundException("Customer not found. ID: " + createSalesOrderRequest.customerId);
        }

        SalesOrder order = this.orderService.createNewOrder(customer);

        return ResponseHelper.createCreatedResponse(order);
    }

    @PutMapping("/addItemsToOrder")
    public ResponseEntity addItemsToOrder(@RequestBody AddItemsToOrderRequest addItemsToOrderRequest) {
        try {
            SalesOrder order = this.orderService.getSalesOrderById(addItemsToOrderRequest.orderId);

            this.orderService.addItemsToOrder(addItemsToOrderRequest);

            return ResponseEntity.ok("Items added to order");
        } catch (OrderNotFoundException e) {
            return ResponseHelper.createErrorResponse("Unable to find order by ID: " + addItemsToOrderRequest.orderId);
        } catch (ProductNotFoundException e) {
            return ResponseHelper.createErrorResponse("One of the products in the request was not found. Exception: " + e.getMessage());
        }

    }
}
