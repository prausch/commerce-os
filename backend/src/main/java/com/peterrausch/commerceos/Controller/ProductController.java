package com.peterrausch.commerceos.Controller;

import com.peterrausch.commerceos.DTO.Product.CreateProductRequest;
import com.peterrausch.commerceos.Exception.SkuExistsException;
import com.peterrausch.commerceos.Helper.ResponseHelper;
import com.peterrausch.commerceos.Model.Product;
import com.peterrausch.commerceos.Service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/v1/product")
public class ProductController {

    private ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/")
    public ResponseEntity getAllProducts() {
        List<Product> products = this.productService.getAllProducts();

        return ResponseEntity.ok(products);
    }

    @GetMapping("/{id}")
    public ResponseEntity getProductById(@PathVariable long id) {
        Product product = this.productService.getProductById(id);

        if (product == null) {
            return ResponseHelper.createErrorResponse("Product not found by ID: " + id);
        }

        return ResponseEntity.ok(product);
    }

    @GetMapping("/sku/{sku}")
    public ResponseEntity getProductBySku(@PathVariable String sku) {
        Product product = this.productService.getProductBySku(sku);

        if (product == null) {
            return ResponseHelper.createErrorResponse("Product not found by SKU: " + sku);
        }

        return ResponseEntity.ok(product);
    }

    @PostMapping("/")
    public ResponseEntity createProduct(@RequestBody CreateProductRequest createProductRequest) {
        try {
            Product product =  this.productService.createProduct(createProductRequest);
            return ResponseHelper.createCreatedResponse(product);
        } catch (SkuExistsException e) {
            return ResponseHelper.createErrorResponse("Sku (" + createProductRequest.sku + ") already exists.");
        }

    }
}
