package com.peterrausch.commerceos.Controller;

import com.peterrausch.commerceos.DTO.Inventory.CreateInventoryAdjustmentRequest;
import com.peterrausch.commerceos.DTO.Inventory.CreateShipmentRequest;
import com.peterrausch.commerceos.Exception.InvalidShipmentRequest;
import com.peterrausch.commerceos.Exception.ProductNotFoundException;
import com.peterrausch.commerceos.Helper.ResponseHelper;
import com.peterrausch.commerceos.Model.InventoryAdjustment;
import com.peterrausch.commerceos.Model.Shipment;
import com.peterrausch.commerceos.Service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/v1/inventory")
public class InventoryController {

    private InventoryService inventoryService;

    @Autowired
    public InventoryController(InventoryService inventoryService) {
        this.inventoryService = inventoryService;
    }

    @PostMapping("/createShipment")
    public ResponseEntity createShipment(CreateShipmentRequest createShipmentRequest) {
        try {
            Shipment newShipment = this.inventoryService.createShipment(createShipmentRequest);

            return ResponseHelper.createCreatedResponse(newShipment);
        } catch (ProductNotFoundException productNotFoundException) {
            return ResponseHelper.createErrorResponse("Product not found. Exception: " + productNotFoundException.getMessage());
        } catch (InvalidShipmentRequest invalidShipmentRequest) {
            return ResponseHelper.createErrorResponse("The shipment request was invalid. Exception: " + invalidShipmentRequest.getMessage());
        }
    }

    @PostMapping("/createInventoryAdjustment")
    public ResponseEntity createInventoryAdjustment(CreateInventoryAdjustmentRequest createInventoryAdjustmentRequest) {
        try {
            InventoryAdjustment inventoryAdjustment = this.inventoryService.createInventoryAdjustment(createInventoryAdjustmentRequest);

            return ResponseHelper.createCreatedResponse(inventoryAdjustment);
        } catch (ProductNotFoundException productNotFoundException) {
            return ResponseHelper.createErrorResponse("Unable to find one of the products in the passed array.  Exception: " + productNotFoundException.getMessage());
        }
    }
}
