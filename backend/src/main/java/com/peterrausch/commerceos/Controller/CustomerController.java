package com.peterrausch.commerceos.Controller;

import com.peterrausch.commerceos.DTO.Customer.CreateCustomerRequest;
import com.peterrausch.commerceos.Helper.ResponseHelper;
import com.peterrausch.commerceos.Model.Customer;
import com.peterrausch.commerceos.Service.CustomerService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/v1/customer")
@Api(value = "CustomerAPI")
public class CustomerController {

    private CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/{id}")
    public ResponseEntity getCustomerById(@PathVariable long id) {
        Customer customer = this.customerService.getCustomerById(id);

        if (customer == null) {
            return ResponseHelper.createErrorResponse("Customer not found");
        }

        return ResponseEntity.ok(customer);
    }

    @PostMapping("/")
    public ResponseEntity createCustomer(@RequestBody CreateCustomerRequest customerRequest) {
        Customer c = this.customerService.createCustomer(customerRequest);

        return ResponseHelper.createCreatedResponse(c);
    }

    @GetMapping("/")
    public ResponseEntity getAllCustomers()  {
        List<Customer> customerList = this.customerService.getAllCustomers();

        return ResponseEntity.ok(customerList);
    }
}
