import { AppConfig } from "@/config/AppConfig";
import Axios, {AxiosRequestConfig, AxiosResponse} from "axios";
import {Product} from "@/interface/Product";

export class ProductService {
    private axiosConfig: AxiosRequestConfig = AppConfig.AXIOS_CONFIG;

    async getAllProducts(): Promise<Product[]> {
        return await Axios.get('/product/', this.axiosConfig).then((res: AxiosResponse<Product[]>) => {
            return res.data;
        }).catch((err: any) => {
            throw err.response.data.message;
        });
    }
}