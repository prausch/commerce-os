import { AppConfig } from "@/config/AppConfig";
import Axios, {AxiosRequestConfig, AxiosResponse} from "axios";
import {Customer} from "@/interface/Customer";

export class CustomerService {
    private axiosConfig: AxiosRequestConfig = AppConfig.AXIOS_CONFIG;

    async getAllCustomers(): Promise<Customer[]> {
        return await Axios.get('/customer/', this.axiosConfig).then((res: AxiosResponse<Customer[]>) => {
            return res.data;
        }).catch((err: any) => {
            throw err.response.data.message;
        });
    }
}