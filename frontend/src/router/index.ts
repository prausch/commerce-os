import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from '../pages/Dashboard.vue';

import Customers from '../pages/Customers.vue';

import Inventory from '../pages/Inventory.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'dashboard',
    component: Dashboard
  },
  {
    path: '/customers/',
    name: 'customers',
    component: Customers
  },
  {
    path: '/inventory/',
    name: 'inventory',
    component: Inventory
  }
];

const router = new VueRouter({
  routes
});

export default router
