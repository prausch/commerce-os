export interface Product {
    id?: number;
    name: string;
    sku: string;
    description: string;
    availableQuantity: number;
    cost: number;
    salePrice: number;
    deleted: boolean;
}