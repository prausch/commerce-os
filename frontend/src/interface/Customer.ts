export interface Customer {
    id?: number;
    firstName: string;
    lastName: string;
    address: string;
    city: string;
    state: string;
    zip: string;
    phoneNumber: string;
    emailAddress: string;
    customerType: string;
    created: string;
}