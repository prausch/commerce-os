export class AppConfig  {
    public static readonly AXIOS_CONFIG = {
        baseURL: "http://localhost:8080/api/v1/",
        headers: {
            "Access-Request-Allow-Origin": "*"
        }
    };
}